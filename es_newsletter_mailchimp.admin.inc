<?php

/**
 * Menu callback: displays the newsletter module settings page.
 *
 * @ingroup forms
 */
function es_newsletter_mailchimp_admin_settings($form) {
  $form['mailchimp_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailchimp API Key'),
    '#default_value' => variable_get('mailchimp_api_key'),
    '#required' => TRUE,
  );
  $form['mailchimp_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailchimp Server'),
    '#default_value' => variable_get('mailchimp_server'),
    '#required' => TRUE,
  );
  $form['mailchimp_list_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailchimp List ID'),
    '#default_value' => variable_get('mailchimp_list_id'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}